import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import Layout from "../components/Layout";
import { TreeTable } from "primereact/treetable";
import { Column } from "primereact/column";
import "./Dark-reader.css";
const client = axios.create({
  baseURL: "https://" + process.env.REACT_APP_USM_GRAFANA_FQDN + ":3451",
});

function UsmList() {
  const [listeClients, setlisteClients] = React.useState(null);
  const [expandedKeys, setExpandedKeys] = useState(null);

  useEffect(() => {
    fetchlisteClients();
  }, []);

  const fetchlisteClients = () => {
    client.get("/listeClientsTable/1").then((response) => {
      setlisteClients(response.data);
      if (!expandedKeys) {
        setExpandedKeys({ [response.data[0].key]: true });
      }
    });
  };

  const actionTemplate = (node) => {
    return (
      <div className="flex flex-wrap gap-2">
        <Link className="btn btn-outline-primary" to={`/UsmEdit/${node.key}`}>
          Edit
        </Link>
        <button
          onClick={() => handleDelete(node.key)}
          className="btn btn-outline-danger mx-1"
        >
          Delete
        </button>
      </div>
    );
  };

  const handleDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        client
          .delete(`/deleteFromId/${id}`)
          .then(function (response) {
            Swal.fire({
              icon: "success",
              title: "Project deleted successfully!",
              showConfirmButton: false,
              timer: 1500,
            });
            fetchlisteClients();
          })
          .catch(function (error) {
            Swal.fire({
              icon: "error",
              title:
                "An Error Occured!  \n" +
                JSON.stringify(error.response.data.detail),
              showConfirmButton: false,
              timer: 1500,
            });
          });
      }
    });
  };
  const expandAll = () => {
    setlisteClients(
      listeClients.map((children) =>
        Object.assign({}, children, {
          expanded: true,
        })
      )
    );
  };
  const collapseAll = () => {
    setlisteClients(
      listeClients.map((children) =>
        Object.assign({}, children, {
          expanded: false,
        })
      )
    );
  };
  const onExpandChange = (event) => {
    const updatedlisteClients = listeClients.slice();
    const childrenIndex = updatedlisteClients.indexOf(event.children);
    updatedlisteClients[childrenIndex] = {
      ...event.children,
    };
    updatedlisteClients[childrenIndex].expanded =
      !updatedlisteClients[childrenIndex].expanded;
    setlisteClients(updatedlisteClients);
  };
  return (
    <Layout>
      <div className="container">
        <h2 className="text-center mt-5 mb-3">Listes des clients :</h2>
        <div className="card">
          <div className="card-header">
            <Link className="btn btn-outline-primary" to="/UsmCreate">
              Create New Project
            </Link>
          </div>
          <div className="card-body">
            <button
              onClick={expandAll}
              className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
            >
              Expand all
            </button>
            <button
              onClick={collapseAll}
              className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
            >
              Collapse all
            </button>
            <TreeTable
              value={listeClients}
              expandedKeys={expandedKeys}
              onToggle={(e) => setExpandedKeys(e.value)}
              tableStyle={{ minWidth: "50rem" }}
              onExpandChange={onExpandChange}
            >
              <Column field="name" header="name" expander sortable></Column>
              <Column field="max-usm" header="max-usm" sortable></Column>
              <Column body={actionTemplate} headerClassName="w-10rem" />
            </TreeTable>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default UsmList;
