import { Grid } from "@mui/material";
import Box from "@mui/material/Box";
import axios from "axios";
import * as React from "react";
import { TreeSelect } from "primereact/treeselect";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";
// import "./App.css";
import { validate as isValidUUID } from "uuid";

const client = axios.create({
  baseURL: "https://" + process.env.REACT_APP_USM_GRAFANA_FQDN + ":3451",
});

function UsmConciliator() {
  const [listeClients, setlisteClients] = React.useState(null);
  React.useEffect(() => {
    client.get("/listClientName/1").then((response) => {
      setlisteClients(response.data);
    });
  }, []);

  const [selectedNodeKeys, setSelectedNodeKeys] = React.useState(null);
  const [uuidList, setuuidList] = React.useState([]);
  const [entityList, setentityList] = React.useState([]);

  React.useEffect(() => {
    const uuidListRetrieve = () => {
      const tempUuidList = [];
      if (listeClients) {
        for (let key in selectedNodeKeys) {
          if (selectedNodeKeys[key].checked === true && isValidUUID(key)) {
            tempUuidList.push(key);
          }
        }
      }
      setuuidList(tempUuidList);
    };
    uuidListRetrieve();
  }, [selectedNodeKeys]);

  React.useEffect(() => {
    const sendUuidsToApi = async () => {
      try {
        const response = await client.post("/store_uuids/", uuidList);
      } catch (error) {
        console.error(error);
      }
    };
    sendUuidsToApi();
  }, [uuidList]);

  React.useEffect(() => {
    const handleFetchUUIDs = async () => {
      try {
        const requests = uuidList.map(async (uuid) => {
          const response = await client.get(`/infoFromId/${uuid}`);
          return response.data;
        });

        const entity = await Promise.all(requests);
        setentityList(entity);
      } catch (error) {
        console.error(error);
      }
    };
    handleFetchUUIDs();
  }, [uuidList]);

  return (
    <div>
      <Grid container spacing={4} justifyContent="center" alignItems="center">
        <Grid item xs={12}>
          <Grid
            container
            spacing={1}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={12}>
              {listeClients && (
                <div
                  className="card flex justify-content-center"
                  style={{ width: "100%" }}
                >
                  <TreeSelect
                    value={selectedNodeKeys}
                    onChange={(e) => setSelectedNodeKeys(e.value)}
                    options={listeClients}
                    filter
                    metaKeySelection={false}
                    className="md:w-20rem w-full"
                    selectionMode="checkbox"
                    display="chip"
                    placeholder="Select Items"
                  ></TreeSelect>
                </div>
              )}
            </Grid>

            <Grid item xs={6}></Grid>
            <Grid item xs={12}>
              <Grid
                container
                spacing={1}
                direction="row"
                justifyContent="center"
                alignItems="stretch"
              >
                <Grid
                  item
                  xs={4}
                  xl={2}
                  style={{
                    borderRadius: 35,
                    height: "100%",
                  }}
                >
                  <div
                    style={{
                      paddingLeft: "8px",
                      paddingTop: "4px",
                      paddingBottom: "10px",
                    }}
                  >
                    <strong>XiVO Selected Value:</strong>
                    {entityList.map((key) => (
                      <div>{key.name}</div>
                    ))}
                  </div>
                </Grid>
                <Grid item></Grid>
                <Grid
                  item
                  xs={4}
                  xl={2}
                  style={{
                    borderRadius: 35,
                    height: "100%",
                  }}
                >
                  <div
                    style={{
                      paddingLeft: "8px",
                      paddingTop: "4px",
                      paddingBottom: "10px",
                    }}
                  >
                    <strong>UUID of Selected Value:</strong>
                    {entityList.map((key) => (
                      <div>{key.id}</div>
                    ))}
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6}></Grid>
            {/* <Grid item xs={12} xl={12}>
              <TransformWrapper>
                <TransformComponent>
                    {listeClients && (
                      <OrganizationChart
                        value={listeClients}
                        selectionMode="single"
                      />
                    )}
                </TransformComponent>
              </TransformWrapper>
            </Grid> */}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default function Usm() {
  return (
    <div
      style={{
        height: "100vh",
      }}
    >
      <Box py={3} px={5}>
        {UsmConciliator()}
      </Box>
    </div>
  );
}
