import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import Layout from "../components/Layout";
import { TreeSelect } from "primereact/treeselect";
import "./Dark-reader.css";

const client = axios.create({
  baseURL: "https://" + process.env.REACT_APP_USM_GRAFANA_FQDN + ":3451",
});

function isUUID(uuid) {
  let s = "" + uuid;

  s = s.match(
    "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
  );
  if (s === null) {
    return false;
  }
  return true;
}

function UsmCreate() {
  const [id, setId] = useState(useParams().id);
  const [name, setName] = useState("");
  const [maxUsm, setmaxUsm] = useState();
  const [isSaving, setIsSaving] = useState(false);
  const [type, setType] = useState("xivo");
  const [parentId, setParentId] = useState();
  const [parentObject, setParentObject] = useState("");
  const [listeClients, setListeClients] = React.useState(null);

  //   useEffect(() => {
  //     client
  //       .get(`infoFromId/${id}`)
  //       .then(function (response) {
  //         setName(response.data.name);
  //         setId(response.data.id);
  //         setmaxUsm(response.data.maxUsm);
  //         setType(response.data.type);
  //         setParentId(response.data.parentId);
  //       })
  //       .catch(function (error) {
  //         // Swal.fire({
  //         //   icon: "error",
  //         //   title: "An Error Occured!",
  //         //   showConfirmButton: false,
  //         //   timer: 1500,
  //         // });
  //       });
  //   }, [id]);

  useEffect(() => {
    fetchParentObject();
  }, [parentId]);

  const fetchParentObject = () => {
    client.get("listClientName/1").then((response) => {
      setListeClients(response.data);
    });
  };

  const handleSave = () => {
    setIsSaving(true);
    client
      .post(`create/`, null, {
        params: {
          type: type,
          uuid: id,
          name: name,
          maxUsm: maxUsm,
          parentId: parentId,
        },
      })
      .then(function (response) {
        Swal.fire({
          icon: "success",
          title: "Project updated successfully!",
          showConfirmButton: false,
          timer: 1500,
        });
        setIsSaving(false);
        fetchParentObject();
      })
      .catch(function (error) {
        Swal.fire({
          icon: "error",
          title:
            "An Error Occured!  \n" +
            JSON.stringify(error.response.data.detail),
          showConfirmButton: false,
          timer: 1500,
        });
        setIsSaving(false);
      });
  };

  const handleType = (type) => {
    setType(type);
  };

  return (
    <Layout>
      <div className="container">
        <h2 className="text-center mt-5 mb-3">
          Create a new {type}
        </h2>
        <div className="card">
          <div className="card-header">
            <Link className="btn btn-outline-info float-right" to="/UsmList">
              View All Entity and XiVO
            </Link>
          </div>
          <div className="card-body">
            Create Xivo or an entity ?<br></br>
            <button
              onClick={() => handleType("xivo")}
              className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
            >
              Xivo
            </button>
            <button
              onClick={() => handleType("entity")}
              className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
            >
              Entity
            </button>
            <form>
              <div className="form-group">
                <label htmlFor="name">Name*</label>
                <input
                  onChange={(event) => {
                    setName(event.target.value);
                  }}
                  value={name}
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                />
              </div>
              {type == "entity" && (
                <div className="form-group">
                  <label htmlFor="maxUsm">
                    Maximum user allow by USM (if an entity)
                  </label>
                  <input
                    value={maxUsm}
                    onChange={(event) => {
                      setmaxUsm(event.target.value);
                    }}
                    className="form-control"
                    id="maxUsm"
                    rows="3"
                    name="maxUsm"
                  />
                </div>
              )}
              {type == "xivo" && (

              <div className="form-group">
                <label htmlFor="maxUsm">XiVO UUID :</label>
                <input
                  value={id}
                  onChange={(event) => {
                    setId(event.target.value);
                  }}
                  className="form-control"
                  id="maxUsm"
                  rows="3"
                  name="maxUsm"
                />
              </div>
              )}

              {listeClients && (
                <div className="form-group">
                  <label htmlFor="maxUsm">Parent*</label>

                  <div
                    className="card flex justify-content-center"
                    style={{ width: "100%" }}
                  >
                    <TreeSelect
                      value={parentId}
                      onChange={(e) => setParentId(e.value)}
                      options={listeClients}
                      filter
                      metaKeySelection={false}
                      className="md:w-20rem w-full"
                      display="chip"
                      placeholder="Select Items"
                    ></TreeSelect>
                  </div>
                </div>
              )}

              <button
                disabled={isSaving}
                onClick={handleSave}
                type="button"
                className="btn btn-outline-success mt-3"
              >
                Add {(type == "xivo"  && "this XiVO") || "this entity"}
              </button>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default UsmCreate;
