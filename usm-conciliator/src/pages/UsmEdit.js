import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import Layout from "../components/Layout";
import { TreeSelect } from "primereact/treeselect";
import "./Dark-reader.css";
import { useNavigate } from "react-router-dom";

const client = axios.create({
  baseURL: "https://" + process.env.REACT_APP_USM_GRAFANA_FQDN + ":3451",
});

function ProjectEdit() {
  const [initialId, setInitialId] = useState(useParams().id);
  const [id, setId] = useState(useParams().id);
  const [name, setName] = useState();
  const [maxUsm, setmaxUsm] = useState("");
  const [isSaving, setIsSaving] = useState(false);
  const [type, setType] = useState("");
  const [parentId, setParentId] = useState();
  const [parentObject, setParentObject] = useState("");
  const [listeClients, setListeClients] = React.useState(null);

  useEffect(() => {
    client
      .get(`infoFromId/${initialId}`)
      .then(function (response) {
        setName(response.data.name);
        setId(response.data.id);
        setmaxUsm(response.data.maxUsm);
        setType(response.data.type);
        setParentId(response.data.parentId);
      })
      .catch(function (error) {
        // Swal.fire({
        //   icon: "error",
        //   title: "An Error Occured!",
        //   showConfirmButton: false,
        //   timer: 1500,
        // });
      });
  }, [initialId]);

  useEffect(() => {
    parentId && fetchParentObject();
  }, [parentId]);

  const fetchParentObject = () => {
    client.get(`infoFromId/${parentId}`).then((response) => {
      setParentObject(response.data);
    });
    client.get("listClientName/1").then((response) => {
      setListeClients(response.data);
    });
  };

  const handleSave = () => {
    setIsSaving(true);
    client
      .put(`updateFromId/${initialId}`, null, {
        params: {
          type: type,
          id: id,
          name: name,
          maxUsm: maxUsm,
          parentId: parentId,
        },
      })
      .then(function (response) {
        Swal.fire({
          icon: "success",
          title: "Project updated successfully!",
          showConfirmButton: false,
          timer: 1500,
        });
        setIsSaving(false);
      })
      .catch(function (error) {
        Swal.fire({
          icon: "error",
          title: "An Error Occured!",
          showConfirmButton: false,
          timer: 1500,
        });
        setIsSaving(false);
      });
  };

  return (
    <Layout>
      <div className="container">
        <h2 className="text-center mt-5 mb-3">
          Edit {type} : {name}
        </h2>
        <div className="card">
          <div className="card-header">
            <Link className="btn btn-outline-info float-right" to="/UsmList">
              View All Entity and XiVO
            </Link>
          </div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  onChange={(event) => {
                    setName(event.target.value);
                  }}
                  value={name}
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                />
              </div>
              {type != "xivo" && (
                <div className="form-group">
                  <label htmlFor="maxUsm">Maximum user allow by USM</label>
                  <input
                    value={maxUsm}
                    onChange={(event) => {
                      setmaxUsm(event.target.value);
                    }}
                    className="form-control"
                    id="maxUsm"
                    rows="3"
                    name="maxUsm"
                  />
                </div>
              )}
              {type == "xivo" && (
                <div className="form-group">
                  <label htmlFor="maxUsm">XiVO UUID :</label>
                  <input
                    value={id}
                    onChange={(event) => {
                      setId(event.target.value);
                    }}
                    className="form-control"
                    id="maxUsm"
                    rows="3"
                    name="maxUsm"
                  />
                </div>
              )}
              {listeClients && parentId && (
                <div className="form-group">
                  <label htmlFor="maxUsm">Parent :</label>

                  <div
                    className="card flex justify-content-center"
                    style={{ width: "100%" }}
                  >
                    <TreeSelect
                      value={parentId}
                      onChange={(e) => setParentId(e.value)}
                      options={listeClients}
                      filter
                      metaKeySelection={false}
                      className="md:w-20rem w-full"
                      display="chip"
                      placeholder="Select Items"
                    ></TreeSelect>
                  </div>
                </div>
              )}

              <button
                disabled={isSaving}
                onClick={handleSave}
                type="button"
                className="btn btn-outline-success mt-3"
              >
                Update {(type == "xivo" && "this XiVO") || "this entity"}
              </button>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default ProjectEdit;
