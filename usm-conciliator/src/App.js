import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Usm from "./pages/Usm";
import UsmList from "./pages/UsmList";
import UsmEdit from "./pages/UsmEdit";
import UsmCreate from "./pages/UsmCreate";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Usm />} />
        <Route path="/UsmCreate" element={<UsmCreate />} />
        <Route path="/UsmList" element={<UsmList />} />
        <Route path="/UsmEdit/:id" element={<UsmEdit />} />
      </Routes>
    </Router>
  );
}

export default App;
