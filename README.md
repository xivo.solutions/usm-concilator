# USM Backend
### Postgres Database diagram
![Postgres Database diagram](dbDiagram.png)
## Installation

On the backend server create the structure:
```bash
mkdir -p /etc/usm-backend/nginx/{ssl,static}
```

"Deploy" the repo:
- from here, in the repo run:
```bash
TAG_OR_BRANCH=master
cd /etc/usm-backend/
wget "https://gitlab.com/xivo.solutions/usm-backend/-/archive/${TAG_OR_BRANCH}/${TAG_OR_BRANCH}.tar.gz"
tar -zxvf ${TAG_OR_BRANCH}.tar.gz -C /etc/usm-backend/ --strip-components 1
rm ${TAG_OR_BRANCH}.tar.gz
```

## Configuration

### SSL

Add the certificates (corresponding to your domain):
:warning: On the PROD don't touch this. This is generated, renewed and pushed by our firewall.
- In `/etc/usm-backend/nginx/ssl` add the SSL certificate:
  - add the cert in file: `usm-backend.crt`
  - add the cert key in file: `usm-backend.key`

### Environment variables
You need to have a .env file with the following variables
Dont forget to replace MyBeautifulApiKey with you grafana API key
Create `.env` file:

```bash
function generatePassword() {
    local length="${1:-20}"; shift
    openssl rand -hex ${length}
}

ENV_FILE_PATH=/etc/usm-backend/.env
DOCKER_INFLUXDB_INIT_PASSWORD=$(generatePassword)
DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=$(generatePassword 64)
GF_SECURITY_ADMIN_PASSWORD=$(generatePassword)
DATABASE_PASSWORD=$(generatePassword)


cat > ${ENV_FILE_PATH} << EOF
USM_FOLDER=/etc/usm-backend
USM_GRAFANA_FQDN=usm.xivo.solutions
USM_INFLUX_FQDN=anonymized-stats.xivo.solutions
DOCKER_INFLUXDB_INIT_USERNAME=xadmin
DOCKER_INFLUXDB_INIT_PASSWORD=${DOCKER_INFLUXDB_INIT_PASSWORD}
DOCKER_INFLUXDB_INIT_ORG=xivo
DOCKER_INFLUXDB_INIT_BUCKET=usm
DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=${DOCKER_INFLUXDB_INIT_ADMIN_TOKEN}
GF_SECURITY_ADMIN_USER=admin
GF_SECURITY_ADMIN_PASSWORD=${GF_SECURITY_ADMIN_PASSWORD}

USM_GRAFANA_ADMIN_TOKEN=MyBeautifulApiKey
DATABASE_USER=postgres
DATABASE_PASSWORD=${DATABASE_PASSWORD}
DATABASE_URL=postgresql://${DATABASE_USER}:${DATABASE_PASSWORD}@db:5432

EOF
```

Create alias:
```bash
echo "alias usm-dcomp='docker-compose -p usm -f /etc/usm-backend/docker-compose.yml --env-file=/etc/usm-backend/.env'" >> ~/.bashrc
source ~/.bashrc
```



### Generate influx token

1. Login with admin user
2. Go to Api Tokens page (arrow up icon) and generate a token name 'XiVO Usage writter' with write acces on 'usm' bucket then copy it.
3. Put it in `/etc/usm-backend/nginx/static/token.txt`

### Add a task in influxdb for removing duplicates

This step can be skipped, if you have already configured the duplicate removal task in influxdb.  

1. In web browser navigate to anonymized-stats.xivo.solutions & login using DOCKER_INFLUXDB_INIT_USERNAME, DOCKER_INFLUXDB_INIT_PASSWORD.
1. Go to Tasks > Create task > New task
1. Fill the form with the following values:

```
Name:           remove_duplicates
Schedule task:  EVERY
Every:          24h
Offset:         -
Task IDE:
TODO: add script for duplicate removal task.
```

## Install in dev environment

On your **laptop**:
1. Go to the root of this repo
2. Create at the root of the project a custom env file:
   ```
   touch my-dev-env
   ```
1. Generate env file as explained above (§Environment variables) **but taking care to generate in `my-dev-env` file
2. Create dirs for nginx:
   ```bash
   mkdir -p nginx/{ssl,static}
   ```
1. Download test certificates:
   ```bash
   wget "https://it-tools.avencall.com/files/certificats_internes/_.test.avencall.com.fullchain" -O nginx/ssl/usm-backend.crt
   wget "https://it-tools.avencall.com/files/certificats_internes/_.test.avencall.com.key" -O nginx/ssl/usm-backend.key
   ```
1. Edit the `my-dev-env` and change the USM_INFLUX_FQDN and USM_GRAFANA_FQDN to, respectively:
   1. USM_INFLUX_FQDN=anonymized-stats.test.avencall.com
   1. USM_GRAFANA_FQDN=usm.test.avencall.com
   2. USM_FOLDER=.
2. On your host add these FQDN to your `/etc/hosts` file:
   ```
   localhost       usm.test.avencall.com anonymized-stats.test.avencall.com
   ```

Then in the **XiVO**:
1. Add USM_BACKEND_URL in the `/etc/docker/xivo/custom.env` file:
   ```bash
   echo "USM_BACKEND_URL=https://anonymized-stats.test.avencall.com" >> /etc/docker/xivo/custom.env
   ```
2. Add this FQDN to your XIVO `/etc/hosts` file **replace 10.32.0.1 by your laptop IP address accessible to your XiVO**:
   ```
   10.32.0.1       anonymized-stats.test.avencall.com
   ```
3. Add an override for usage_writer to know where is the anonymized-stats backend **replace 10.32.0.1 by your laptop IP address accessible to your XiVO**:

```bash
cat > /etc/docker/xivo/22-usmdev.override.yml << EOF
version: "3.7"

services:
  usage_collector:

    extra_hosts:
    - "anonymized-stats.test.avencall.com:10.32.0.1"
EOF
```

On your **laptop**:
1. Start the backend:
   ```
   docker-compose -f docker-compose.yml -p usmdev --env-file=my-dev-env up
   ```
2. Generate the token and copy it in `nginx/static/token.txt` (see §Generate influx token)

On your **XiVO**:
1. Put token in custom.env
   ```bash
   token=$(curl --fail --connect-timeout 10 --max-time 20 "https://anonymized-stats.test.avencall.com/usm/token")
   echo "USM_BACKEND_TOKEN=${token}" >> /etc/docker/xivo/custom.env
   ```
3. Restart usage_writer*
   ```bash
   xivo-dcomp up -d usage_writer usage_collector
   ```

Now, you can access to :

- https://usm.test.avencall.com:3448 for Grafana
- https://usm.test.avencall.com:3449 for InfluxDB
- https://usm.test.avencall.com:3450 for React App
- https://usm.test.avencall.com:3451 for python FastApi
- https://usm.test.avencall.com:3451/docs FastApi swager


