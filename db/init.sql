CREATE TABLE entity(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    max_usm INTEGER,
    parent_id INTEGER REFERENCES entity(id)
)

CREATE TABLE xivo (
    xivo_uuid uuid PRIMARY KEY,
    name VARCHAR NOT NULL,
    parent_id INTEGER REFERENCES entity(id)
)

CREATE VIEW EntityParentChildren AS
SELECT
    e1.id AS parent_id,
    e2.id AS child_id
FROM
    entity e1
JOIN
    entity e2 ON e2.parent_id = e1.id;


CREATE VIEW XivoParentChildren AS
SELECT
    e1.id AS parent_id,
    x2.xivo_uuid AS child_id
FROM
    entity e1
JOIN
    xivo x2 ON x2.parent_id = e1.id;

INSERT INTO entity
(id, "name", max_usm, parent_id)
VALUES(1, 'Root', NULL, NULL);
