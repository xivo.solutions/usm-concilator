from fastapi import FastAPI, Depends, HTTPException, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from app.Endpoints.hello import hello
from app.Endpoints.clientTree import listeClients
from app.Endpoints.clientTree import listeClientsTable
from app.Endpoints.clientTree import ClientNameFromUuid
from app.Endpoints.clientTree import infoFromId
from app.Endpoints.clientTree import deleteFromId
from app.Endpoints.clientTree import updateFromId
from app.Endpoints.clientTree import create


app = FastAPI()
security = HTTPBearer()

# CORS configuration
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return hello()


@app.get("/infoFromId/{id}", tags=["API for usm-concilator"])
async def get_infoFromClientName(id):
    return infoFromId(id)


@app.get("/ClientNameFromUuid/{id}", tags=["API for usm-concilator"])
async def get_ClientNameFromUuid(id):
    return ClientNameFromUuid(id)


uuid_list = []


@app.post("/store_uuids/", tags=["API for usm-concilator"])
async def store_uuids(uuids: list[str]):
    global uuid_list
    uuid_list = uuids
    return {"message": "UUIDs stored successfully"}


@app.get("/get_uuids/", tags=["API for usm-concilator"])
async def get_uuids():
    global uuid_list
    uuid_dict = {uuid: uuid for uuid in uuid_list}
    return uuid_dict


@app.get("/listClientName/{id}", tags=["API for usm-concilator"])
def read_listClientName(id):
    return listeClients(id)


@app.get("/listeClientsTable/{id}", tags=["API for usm-concilator"])
def read_listeClientsTable(id):
    return listeClientsTable(id)


@app.delete("/deleteFromId/{id}", tags=["API for usm-concilator"])
async def delete_deleteFromId(id):
    return deleteFromId(id)


@app.put("/updateFromId/{idOrigin}", tags=["API for usm-concilator"])
async def put_updateFromId(
    idOrigin, type: str, id, name: str, parentId: int, maxUsm: int | None = None
):
    return updateFromId(idOrigin, type, id, name, parentId, maxUsm)


@app.post("/create/", tags=["API for usm-concilator"])
async def post_create(
    type: str, name: str, parentId: int, maxUsm: int | None = None, uuid: str | None = None
):
    if type == "entity":
        return create(type= type, name= name, parentId= parentId, maxUsm= maxUsm)
    elif type == "xivo":
        return create(type=type, uuid= uuid, name= name, parentId= parentId)
    else:
        return {"message": "error"}


