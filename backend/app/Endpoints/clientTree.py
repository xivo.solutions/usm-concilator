#!/usr/bin/env python3

import os
import json
import uuid
from sqlalchemy import create_engine, Column, Integer, String, JSON, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import func, text
from fastapi import FastAPI, Request, status, HTTPException
from sqlalchemy import exc
from fastapi.responses import Response
from dotenv import load_dotenv

from app.Models.models import Entity, Xivo

# Charger les variables d'environnement depuis le fichier .env si nécessaire
load_dotenv()

# Connexion à la base de données PostgreSQL
DATABASE_URL = os.environ.get("DATABASE_URL")
engine = create_engine(DATABASE_URL, pool_size=40)
Base = declarative_base()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))

        return True
    except ValueError:
        return False


def ClientNameFromUuid(id):
    session = SessionLocal()

    if is_valid_uuid(id):
        xivo = session.query(Xivo).filter(Xivo.xivo_uuid == id).first()
        return {"xivoName": xivo.name}
    else:
        entity = session.query(Entity).filter(Entity.id == id).first()
        return {"entityName": entity.name}


def infoFromId(id):
    session = SessionLocal()

    if is_valid_uuid(id):
        xivo = session.query(Xivo).filter(Xivo.xivo_uuid == id).first()
        return {
            "type": "xivo",
            "id": xivo.xivo_uuid,
            "name": xivo.name,
            "parentId": xivo.parent_id,
        }
    else:
        entity = session.query(Entity).filter(Entity.id == id).first()
        return {
            "type": "entity",
            "id": entity.id,
            "name": entity.name,
            "maxUsm": entity.max_usm,
            "parentId": entity.parent_id,
        }


def listeClients(id):
    rootEntityId = int(id)

    session = SessionLocal()

    entity_parent_children = session.execute(
        text("SELECT parent_id, child_id FROM EntityParentChildren")
    ).fetchall()
    xivo_parent_children = session.execute(
        text("SELECT parent_id, child_id FROM XivoParentChildren")
    ).fetchall()

    def build_tree(data, parent_id=None):
        tree = []
        for parent, child in data:
            if parent == parent_id:
                if type(child) == uuid.UUID:
                    child_entity = session.query(Xivo).get(child)
                    child_key = child_entity.xivo_uuid
                else:
                    child_entity = session.query(Entity).get(child)
                    child_key = child_entity.id

                child_node = {
                    "key": child_key,
                    "label": child_entity.name,
                    "data": child_entity.name,
                    "children": build_tree(data, child),
                }
                tree.append(child_node)
        session.commit()
        return tree

    combined_data = entity_parent_children + xivo_parent_children

    tree = build_tree(combined_data, parent_id=rootEntityId)

    session.commit()
    return tree


def listeClientsTable(id):
    rootEntityId = int(id)
    session = SessionLocal()

    entity_parent_children = session.execute(
        text("SELECT parent_id, child_id FROM EntityParentChildren")
    ).fetchall()
    xivo_parent_children = session.execute(
        text("SELECT parent_id, child_id FROM XivoParentChildren")
    ).fetchall()

    def build_tree(data, parent_id=None):
        tree = []
        for parent, child in data:
            if parent == parent_id:
                if type(child) == uuid.UUID:
                    child_entity = session.query(Xivo).get(child)
                    child_key = child_entity.xivo_uuid
                    max_usm = None
                else:
                    child_entity = session.query(Entity).get(child)
                    child_key = child_entity.id
                    max_usm = child_entity.max_usm

                child_node = {
                    "key": child_key,
                    "data": {"name": child_entity.name, "max-usm": max_usm},
                    "children": build_tree(data, child),
                }
                tree.append(child_node)
        return tree

    combined_data = entity_parent_children + xivo_parent_children

    tree = build_tree(combined_data, parent_id=rootEntityId)

    session.commit()
    return tree


def deleteFromId(id):
    session = SessionLocal()

    if is_valid_uuid(id):
        xivo = session.get(Xivo, id)
        if not xivo:
            raise HTTPException(
                status_code=404, detail=f"Xivo with uuid: {id} , not found"
            )
        try:
            session.delete(xivo)
            session.commit()
        except exc.IntegrityError:
            raise HTTPException(
                status_code=400, detail=f"foreign key constraint violated"
            )

        return {"Deleted": id}

    else:
        entity = session.get(Entity, id)
        if not entity:
            raise HTTPException(
                status_code=404, detail=f"Entity with id {id} not found"
            )
        try:
            session.delete(entity)
            session.commit()
        except exc.IntegrityError:
            raise HTTPException(
                status_code=400, detail=f"foreign key constraint violated"
            )
        return {"Deleted": id}


def updateFromId(idOrigin, type, id, name, parentId, maxUsm):
    session = SessionLocal()
    print(f"idOrigin : {idOrigin}")
    # try:

    if type == "xivo":
        # Check parameter
        if not id:
            raise HTTPException(status_code=400, detail=f"Xivo is a mandatory field")
        if not is_valid_uuid(id):
            raise HTTPException(
                status_code=400, detail=f"Xivo UUID is not a valid uuidv4"
            )

        if not name:
            raise HTTPException(status_code=400, detail=f"Name is a mandatory field")
        if not isinstance(name, str):
            raise HTTPException(
                status_code=404,
                detail=f"name is not a string",
            )

        if not parentId:
            raise HTTPException(
                status_code=400, detail=f"Parentid is a mandatory field"
            )

        db_parent = session.get(Entity, parentId)
        if not db_parent:
            raise HTTPException(
                status_code=404,
                detail=f"Parent with id {parentId} not found, choose real parent",
            )

        # Update xivo
        db_xivo = session.query(Xivo).filter(Xivo.xivo_uuid == idOrigin).first()
        db_xivo.xivo_uuid = id
        db_xivo.name = name
        db_xivo.parent_id = parentId
        session.add(db_xivo)
        session.commit()
        return {"message": "Sucessfully modified xivo"}

    if type == "entity":
        # Check parameter
        if not name:
            raise HTTPException(status_code=400, detail=f"Name is a mandatory field")
        if not isinstance(name, str):
            raise HTTPException(
                status_code=404,
                detail=f"name is not a string",
            )

        if not parentId:
            raise HTTPException(
                status_code=400, detail=f"Parentid is a mandatory field"
            )

        db_parent = session.get(Entity, parentId)
        if not db_parent:
            raise HTTPException(
                status_code=404,
                detail=f"Parent with id {parentId} not found, choose real parent",
            )

        # Update entity
        db_entity = session.query(Entity).filter(Entity.id == idOrigin).first()
        db_entity.id = id
        db_entity.name = name
        db_entity.parent_id = parentId
        db_entity.max_usm = maxUsm
        session.add(db_entity)
        session.commit()
        return {"message": "Sucessfully modified entity"}


def create(type, uuid = None, name = None, parentId = None, maxUsm = None):
    session = SessionLocal()
    print(f"uuid : {uuid}")
    print(f"name : {name}")
    print(f"parentId : {parentId}")
    print(f"maxUsm : {maxUsm}")
    if is_valid_uuid(uuid):
        type = "xivo"
    else:
        type = "entity"

    if type == "xivo":
        if not is_valid_uuid(uuid):
            raise HTTPException(
                status_code=400, detail=f"Xivo UUID is not a valid uuidv4"
            )
        if not name:
            raise HTTPException(status_code=400, detail=f"Name is a mandatory field")
        if not parentId:
            raise HTTPException(
                status_code=400, detail=f"Parentid is a mandatory field"
            )

        xivo = Xivo(xivo_uuid=uuid, name=name, parent_id=parentId)
        try:
            session.add(xivo)
            session.commit()
            session.refresh(xivo)
        except exc.IntegrityError:
            raise HTTPException(
                status_code=400, detail=f"Key (xivo_uuid)=({uuid}) already exists."
            )
        return xivo

    elif type == "entity":
        if not name:
            raise HTTPException(status_code=400, detail=f"Name is a mandatory field")
        if not parentId:
            raise HTTPException(
                status_code=400, detail=f"Parentid is a mandatory field"
            )

        entity = Entity(name=name, parent_id=parentId, max_usm=maxUsm)
        session.add(entity)
        session.commit()
        session.refresh(entity)
        return entity
