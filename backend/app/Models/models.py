from sqlalchemy import create_engine, Column, Integer, String, JSON, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
import uuid

Base = declarative_base()


class Entity(Base):
    __tablename__ = "entity"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    max_usm = Column(Integer)
    parent_id = Column(Integer, ForeignKey("entity.id"))


class Xivo(Base):
    __tablename__ = "xivo"

    xivo_uuid = Column(String, primary_key=True, default=str(uuid.uuid4()))
    name = Column(String, nullable=False)
    parent_id = Column(Integer, ForeignKey("entity.id"))
